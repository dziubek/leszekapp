package co.untitledkingdom.leszekapp.data

data class Rocket(val name: String = "", val photoUrl: String = "")